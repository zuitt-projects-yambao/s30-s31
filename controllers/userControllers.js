const User = require('../models/User');

module.exports.addUserController = (req, res) => 
	{
		console.log(req.body);

		User.findOne({username: req.body.username}).then(result => {

			if(result !==null && result.username === req.body.username){
				return res.send('Duplicate task found');
			}
			else{
				let newUser = new User({
						username: req.body.username,
						password: req.body.password
					});

					newUser.save()
					.then(result => res.send(result))
					.catch(error => res.send(error));
			}

	}).catch(error => res.send(error));
};

module.exports.getAllUsersController = (req, res) => {

	// db.User.find({})
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};



// Activity 2


// Update a username depending on the given id
module.exports.updateUsernameController = (req, res) => {

	let id = req.params.id;

	console.log(id);
	console.log(req.body);

	let update = {
		username: req.body.username 
	};

	User.findByIdAndUpdate(id, update, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));


};


// Retrieve a single user depending on the given id
module.exports.getSingleUserController = (req,res) => {
	console.log(req.params);

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};



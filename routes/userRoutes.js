// importing express to use the Router() method
const express = require('express');

// allows us to access our HTTP method routes
const router = express.Router();

// importing userControllers
const userControllers = require('../controllers/userControllers');

// create a user route
router.post('/', userControllers.addUserController);

// retrieve users data
router.get('/', userControllers.getAllUsersController);


// Activity 2
// update user's username
router.put('/:id', userControllers.updateUsernameController);


// retrieve a single user
router.get('/getSingleUser/:id', userControllers.getSingleUserController);



module.exports = router;

